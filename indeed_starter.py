
from selenium import webdriver
import time
import re
import csv

def scrape_page(some_list):
    '''scrapes each page'''
    all_items_tags = driver.find_elements_by_xpath('//div[@class="jobsearch-SerpJobCard unifiedRow row result clickcard"]')
    with open('ds4_jobs.csv','a', encoding='utf-8', newline='') as csv_file:
        for element in all_items_tags:
            writer = csv.writer(csv_file)
            elements_list = element.text.split('\n')
            writer.writerow(elements_list)

def click_next_page(default=1):
    '''clicks on the next page to load more results'''
    next_button = driver.find_elements_by_xpath('//span[@class="np"]')[default]
    driver.execute_script("arguments[0].scrollIntoView();", next_button)
    time.sleep(2)
    next_button.click()
    
def close_popup():
    '''closes the popup after the next button is clicked the first time'''
    xpath_close_pop_up ='//a[@class="icl-CloseButton popover-x-button-close"]'
    button_2 = driver.find_element_by_xpath(xpath_close_pop_up)
    button_2.click()
    
#open the browser and navigate to page then wait 2 seconds
driver = webdriver.Chrome()
driver.get('https://www.indeed.com/q-Data-Scientist-jobs.html')
time.sleep(2)

#scrape first page, wait 2 seconds, click next page, wait 2 seconds, close popup on second page 
print("scraping first page")
jobs_list =[]
scrape_page(jobs_list)
time.sleep(2)
click_next_page(default=0)
time.sleep(2)
close_popup()

#scrape the first 100 pages:
index = 2
while True:
    print("Scraping Page number " + str(index))
    index = index + 1
    try:
        scrape_page(jobs_list)
        time.sleep(2)
        click_next_page()
    except:
        driver.close()
        break

#scrape from results 1000 to 12500 in incremenets of 10 
for i in range(1000,12500,10):
    print("Scraping page number " + str(i))
    driver.get('https://www.indeed.com/jobs?q=Data+Scientist&filter=0&start={}'.format(i))
    scrape_page(jobs_list)
    time.sleep(2)

print('DONE')
driver.close()



